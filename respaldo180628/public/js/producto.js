$(document).ready(function () {
        var form_register = $('#formproductos');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                pcodigo:{
                  required: true
                },
                productoname:{
                    required: true
                },
                txtUsuario:{
                    required: true
                },
                pstock:{
                    min: 0,
                    required: true
                },
                preciocompra:{
                    required: true
                },
                porcentaje:{
                    required: true
                },
                precioventa:{
                    required: true
                },
                preciommayoreo:{
                    required: true
                },
                cpmmayoreo:{
                    required: true
                },
                preciomayoreo:{
                    required: true
                },
                cpmayoreo:{
                    required: true
                }
                
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });

        //=====================================================================================================
        $('#pcategoria').select2({
            placeholder: {
                id: 0,
                text: 'Seleccione un producto'
            },
            width: '100%',
            allowClear: true
        });
        $("#pcategoria").val(0).change();
        $('#pmarca').select2({
            placeholder: {
                id: 0,
                text: 'Seleccione una marca'
            },
            width: '100%',
            allowClear: true
        });
        $("#pmarca").val(0).change();
        $('#ppresentacion').select2({
            placeholder: {
                id: 0,
                text: 'Seleccione una presentación'
            },
            width: '100%',
            allowClear: true
        });
        $("#ppresentacion").val(0).change();

        $('#savepr').click(function(event) {
            var $valid = $("#formproductos").valid();
            console.log($valid);
            if($valid) {
                $.ajax({
                    type:'POST',
                    url: 'productoadd',
                   // data: {id:id,nom:nom,ape:ape,perf:perf,mail:email,tcel:telcel,tcasa:telcasa,call:calle,next:noexte,est:estado,nint:noint,col:colonia,mun:municipio,cp:cop},
                    data: {
                        id: $('#productoid').val(),
                        cod: $('#pcodigo').val(),
                        pfiscal: $('#productofiscal').is(':checked')==true?1:0,
                        nom: $('#productoname').val(),
                        des:$('#pdescripcion').val(),
                        catego: $('#pcategoria option:selected').val(),
                        stock:$('#pstock').val(),
                        preciocompra: $('#preciocompra').val(),
                        porcentaje: $('#porcentaje').val(),
                        precioventa: $('#precioventa').val(),
                        pmmayoreo: $('#preciommayoreo').val(),
                        cpmmayoreo: $('#cpmmayoreo').val(),
                        pmayoreo: $('#preciomayoreo').val(),
                        cpmayoreo: $('#cpmayoreo').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        var idpro=data;
                        toastr.success('Hecho!', 'Guardado Correctamente');
                        //=====================
                        	if ($('#imgpro')[0].files.length > 0) {
				            	var inputFileImage = document.getElementById('imgpro');
								var file = inputFileImage.files[0];
								var data = new FormData();
								data.append('img',file);
								data.append('carpeta',$('#carpetap').val());
								data.append('idpro',idpro);
								$.ajax({
									url:'imgpro',
									type:'POST',
									contentType:false,
									data:data,
									processData:false,
									cache:false,
									success: function(data) {
										var array = $.parseJSON(data);
							                if (array.ok=true) {
							                	$(".fileinput").fileinput("clear");
							                }else{
							                	toastr.error('Error', data.msg);
							                }
									},
									error: function(jqXHR, textStatus, errorThrown) {
							                var data = JSON.parse(jqXHR.responseText);
							                console.log(data);
							                if (data.ok=='true') {
							                	$(".fileinput").fileinput("clear");
							                }else{
							                	toastr.error('Error', data.msg);
							                }
							              
							            }
								});
				        	}
                        //=====================
                        setInterval(function(){ 
                        	location.href='../Productos';
                        }, 3000);
                        



                        
                    }
                });
            }
        });
});
function calcular(){
	var costo = $('#preciocompra').val();
    var porcentaje = $('#porcentaje').val();
    var porcentaje2 = porcentaje/100;
    var costo2 = costo*porcentaje2;
    var cantitotal = parseFloat(costo)+parseFloat(costo2);
    $('#precioventa').val(cantitotal);
    $('#preciommayoreo').val(cantitotal);
    $('#cpmmayoreo').val('1');
    $('#preciomayoreo').val(cantitotal);
    $('#cpmayoreo').val('1');
}
function buscarproducto(){
    var search=$('#buscarpro').val();
    if (search.length>2) {
        $.ajax({
            type:'POST',
            url: 'Productos/buscarpro',
            data: {
                buscar: $('#buscarpro').val()
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#tbodyresultadospro2').html(data);
            }
        });
        $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
    }else{
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
    }
}
