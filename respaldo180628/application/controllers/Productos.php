<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloCatalogos');
    }
	public function index(){
            $pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'Productos/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloProductos->filas();//calcula el número de filas
            $config['per_page'] = 10; //Número de registros mostrados por páginas  
            $config['num_links'] = 3; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["productos"] = $this->ModeloProductos->total_paginados($pagex,$config['per_page']);
            //$data["productosp"] = $this->ModeloProductos->total_paginadosp($pagex,$config['per_page']);
            $data['total_rows'] = $this->ModeloProductos->filas();
            //$data['totalexistencia'] = $this->ModeloProductos->totalproductosenexistencia();
            //$data['totalproductopreciocompra'] = $this->ModeloProductos->totalproductopreciocompra();
            //$data['totalproductoporpreciocompra'] = $this->ModeloProductos->totalproductoporpreciocompra();

            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('productos/productos',$data);
            $this->load->view('templates/footer');
            $this->load->view('productos/jsproducto');
	}
    public function productosadd(){
        $data["categorias"] = $this->ModeloProductos->categorias();
        $data["marcaall"] = $this->ModeloCatalogos->marcas_all();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/productoadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/jsproducto');
    }
    function productoadd(){
        $id = $this->input->post('id');
        $cod = $this->input->post('cod');
        $pfiscal = $this->input->post('pfiscal');
        $nom = $this->input->post('nom');
        $des = $this->input->post('des');
        $catego = $this->input->post('catego');
        $stock = $this->input->post('stock');
        $preciocompra = $this->input->post('preciocompra');
        $porcentaje = $this->input->post('porcentaje');
        $precioventa = $this->input->post('precioventa');
        $pmmayoreo = $this->input->post('pmmayoreo');
        $cpmmayoreo = $this->input->post('cpmmayoreo');
        $pmayoreo = $this->input->post('pmayoreo');
        $cpmayoreo = $this->input->post('cpmayoreo');
        if ($id>0) {
            $this->ModeloProductos->productosupdate($id,$cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo);
            echo $id;
        }else{
            $idd=$this->ModeloProductos->productosinsert($cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo);
            echo $idd;
        }
    }
    function imgpro(){
        $idpro = $this->input->post('idpro');
        

        $upload_folder ='public/img/productos';
        $nombre_archivo = $_FILES['img']['name'];
        $tipo_archivo = $_FILES['img']['type'];
        $tamano_archivo = $_FILES['img']['size'];
        $tmp_archivo = $_FILES['img']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $tipo_archivo1 = explode('/', $tipo_archivo); 
        $tipoactivo=$tipo_archivo1[1];
        if ($tipoactivo!='png') {
            $tipo_archivo1 = explode('+', $tipoactivo); 
            $tipoactivo=$tipo_archivo1[0];
        }
        $archivador = $upload_folder . '/'.$fecha.'pro.'.$tipoactivo;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $resimg=$this->ModeloProductos->imgpro($archivador,$idpro);
            $return = Array('ok'=>TRUE,'img'=>$tipoactivo);
        }
        echo json_encode($return);
    }
    public function deleteproductos(){
        $id = $this->input->post('id');
        $this->ModeloProductos->productosdelete($id); 
    }
    function buscarpro(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloProductos->productoallsearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trpro_<?php echo $item->productoid; ?>">
                <td>
                    <?php 
                    if ($item->img=='') {
                        $img='public/img/ops.png';
                    }else{
                        $img=$item->img;
                    }?>
                <img src="<?php echo base_url(); ?><?php echo $img; ?>" class="imgpro" >
                </td>
              <td><?php echo $item->nombre; ?></td>
              <td><?php echo $item->descripcion; ?></td>
              <td><?php echo $item->categoria; ?></td>
              <td><?php echo $item->precioventa; ?></td>
              <td><?php echo $item->stock; ?></td>
              <td>
                  <div class="btn-group mr-1 mb-1">
                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                      <a class="dropdown-item" href="<?php echo base_url();?>Productos/productosadd?id=<?php echo $item->productoid; ?>">Editar</a>
                      <a class="dropdown-item" onclick="etiquetas(<?php echo $item->productoid; ?>,'<?php echo $item->codigo; ?>','<?php echo $item->nombre; ?>','<?php echo $item->categoria; ?>',<?php echo $item->precioventa; ?>);"href="#">Etiquetas</a>
                      <a class="dropdown-item" onclick="productodelete(<?php echo $item->productoid; ?>);"href="#">Eliminar</a>
                  </div>
              </div>
              </td>
            </tr>
        <?php }
    }
    
    

       
    
}
