<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
    }/*
	public function index(){
        $data['sturno']=$this->ModeloVentas->turnos();
        $data['clientedefault']=$this->ModeloVentas->clientepordefecto();
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas/ventas',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas/jsventas');
	}*/
    public function index(){
        //$data['sturno']=$this->ModeloVentas->turnos();
        //$data['clientedefault']=$this->ModeloVentas->clientepordefecto();
        $data['categoriasll']=$this->ModeloCatalogos->categorias_all();
        $data['marcasll']=$this->ModeloCatalogos->marcashuevo_all();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        //$this->load->view('ventas/ventas',$data);
        $this->load->view('ventas/ventas2',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas/jsventas');
    }
    /*
    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloClientes->clientesallsearch($usu);
        echo json_encode($results->result());
    }
    public function searchproducto(){
        $pro = $this->input->get('search');
        $results=$this->ModeloProductos->productoallsearch($pro);
        echo json_encode($results->result());
    }
    public function addproducto(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $personalv=$this->ModeloProductos->getproducto($prod);
        //$oProducto = new Producto();
        foreach ($personalv->result() as $item){
              $id = $item->productoid;
              $codigo = $item->codigo;
              $nombre = $item->nombre;
              $precioventa = $item->precioventa;
              $mediomayoreo = $item->mediomayoreo;
              $canmediomayoreo = $item->canmediomayoreo;
              $mayoreo = $item->mayoreo;
              $canmayoreo = $item->canmayoreo;

                //$oProducto->setid($id);
                //$oProducto->setcodigo($codigo);
                //$oProducto->setnombre($nombre);
                //$oProducto->setprecioventa($precioventa);
                //$oProducto->setmediomayoreo($mediomayoreo);
                //$oProducto->setcanmediomayoreo($canmediomayoreo);
                //$oProducto->setmayoreo($mayoreo);
                //$oProducto->setcanmayoreo($canmayoreo);
                $oProducto=array("id"=>$id,"codigo"=>$codigo,"nombre"=>$nombre,'precioventa'=>$precioventa,'mediomayoreo'=>$mediomayoreo,'canmediomayoreo'=>$canmediomayoreo,'mayoreo'=>$mayoreo,'canmayoreo'=>$canmayoreo);

        }
        if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
            $idx = array_search($oProducto, $_SESSION['pro']);
            $_SESSION['can'][$idx]+=$cant;
        }
        else{ //sino lo agrega
            array_push($_SESSION['pro'],$oProducto);
            array_push($_SESSION['can'],$cant);
        }
        //======================================================================
        $count = 0;
        $n =array_sum($_SESSION['can']);
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count]; 
        
        if ($Cantidad>=$fila['canmayoreo']) {
            $precio=$fila['mayoreo'];
        }elseif ($Cantidad>=$fila['canmediomayoreo'] && $Cantidad<$fila['canmayoreo']) {
            $precio=$fila['mediomayoreo'];
        }else{
            $precio=$fila['precioventa'];
        }
        $cantotal=$Cantidad*$precio;
        ?>
            <tr class="producto_<?php echo $count;?>">                                        
                <td>
                    <input type="hidden" name="vsproid" id="vsproid" value="<?php echo $fila['id'];?>">
                    <?php echo $fila['codigo'];?>
                </td>                                        
                <td>
                    <input type="number" name="vscanti" id="vscanti" value="<?php echo $Cantidad;?>" readonly style="background: transparent;border: 0px; width: 80px;">
                </td>                                        
                <td><?php echo $fila['nombre'];?></td>                                        
                <td>$ <input type="text" name="vsprecio" id="vsprecio" value="<?php echo $precio;?>" readonly style="background: transparent;border: 0px;width: 100px;"></td>                                        
                <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="<?php echo $cantotal;?>" readonly style="background: transparent;border: 0px;    width: 100px;"></td>                                        
                <td>                                            
                    <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro(<?php echo $count;?>)">
                        <i class="ft-trash font-medium-3"></i>
                    </a>
                </td>
            </tr>
        <?php
        $count++;
        }
    }
    function productoclear(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
    }
    function deleteproducto(){
        $idd = $this->input->post('idd');
        unset($_SESSION['pro'][$idd]);
        $_SESSION['pro']=array_values($_SESSION['pro']);
        unset($_SESSION['can'][$idd]);
        $_SESSION['can'] = array_values($_SESSION['can']); 
    }
    function ingresarventa(){
        $uss = $this->input->post('uss');
        $cli = $this->input->post('cli');
        $mpago = $this->input->post('mpago');
        $desc = $this->input->post('desc');
        $descu = $this->input->post('descu');
        $sbtotal = $this->input->post('sbtotal');
        $total = $this->input->post('total');
        $efectivo = $this->input->post('efectivo');
        $tarjeta = $this->input->post('tarjeta');
        $id=$this->ModeloVentas->ingresarventa($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$tarjeta);
        echo $id;
    }
    function ingresarventapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idventa = $DATA[$i]->idventa;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $precio = $DATA[$i]->precio;
            $this->ModeloVentas->ingresarventad($idventa,$producto,$cantidad,$precio);
        }
    }
    function abrirturno(){
        $cantidad = $this->input->post('cantidad');
        $nombre = $this->input->post('nombre');
        $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
        $horaa =date('H:i:s');
        $this->ModeloVentas->abrirturno($cantidad,$nombre,$fecha,$horaa);
    }
    function cerrarturno(){
        $id = $this->input->post('id');
        $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
        $horaa =date('H:i:s');
        $this->ModeloVentas->cerrarturno($id,$horaa);
    }
    function consultarturno(){
        $id = $this->input->post('id');
        $vturno=$this->ModeloVentas->consultarturno($id);
        foreach ($vturno->result() as $item){
            $fecha= $item->fecha;
            $fechac= $item->fechacierre;
            $horaa= $item->horaa;
            $horac= $item->horac;
        }
        if ($horac=='00:00:00') {
            $horac =date('H:i:s');
            $fechac =date('Y-m-d');
        }
        $totalventas=$this->ModeloVentas->consultartotalturno($fecha,$horaa,$horac,$fechac);
        echo "<p><b>Total: ".number_format($totalventas,2,'.',',')."</b></p>";
        $totalpreciocompra=$this->ModeloVentas->consultartotalturno2($fecha,$horaa,$horac,$fechac);
        $obed =$totalventas-$totalpreciocompra;
        echo "<p><b>Utilidad: ".number_format($obed,2,'.',',')."</b></p>";
        $productos =$this->ModeloVentas->consultartotalturnopro($fecha,$horaa,$horac,$fechac);
        echo "<div class='panel-body table-responsive' id='table'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables2'>
                            <thead class='vd_bg-green vd_white'>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    
                                </tr>
                            </thead>
                            <tbody>";
        foreach ($productos->result() as $item){
            echo "<tr><td>".$item->producto."</td><td>".$item->cantidad."</td><td> $".$item->precio."</td></tr>";
        }
        echo "</tbody> </table></div>";
        echo "<div style='    position: absolute; float: left; top: 0; left: 800px; background: white; width: 250px; z-index: 20;'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables'>
                            <thead>
                                <tr>
                                    <td colspan='4' style='text-align: center;'>Productos mas vendido</td>
                                </tr>
                            </thead>
                            <tbody>";
        $productosmas =$this->ModeloVentas->consultartotalturnopromas($fecha,$horaa,$horac,$fechac);
        foreach ($productosmas->result() as $item){
            echo "<tr><td colspan='2' style='text-align: center;'>".$item->total." </td><td colspan='2' style='text-align: center;'>".$item->producto." </td></tr>";
        }
        echo "</tbody> </table></div>";

    }*/

}