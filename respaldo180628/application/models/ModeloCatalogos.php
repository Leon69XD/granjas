<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    //====================== categoria===================================
    function categorias_all() {
        $strq = "SELECT * FROM categoria where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categoriadell($id){
        $strq = "UPDATE categoria SET activo=0 WHERE categoriaId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categoriaadd($nom){
        $strq = "INSERT INTO categoria(categoria) VALUES ('$nom')";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        
        return $id;
    }
    function categoriupdate($nom,$id){
        $strq = "UPDATE categoria SET categoria='$nom' WHERE categoriaId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function categoriaaddimg($img,$id){
        $strq = "UPDATE categoria SET img='$img' WHERE categoriaId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    //=======================fin categoria================
    //====================== marcas===================================
    function marcas_all() {
        $strq = "SELECT * FROM marca where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function marcadell($id){
        $strq = "UPDATE marca SET activo=0 WHERE marcaid=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function marcaadd($nom){
        $strq = "INSERT INTO marca(marca) VALUES ('$nom')";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        
        return $id;
    }
    function marcaupdate($nom,$id){
        $strq = "UPDATE marca SET marca='$nom' WHERE marcaid=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function marcaaddimg($img,$id){
        $strq = "UPDATE marca SET imgm='$img' WHERE marcaid=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    //=======================fin marcas================
    function updatenota($use,$nota){
        $strq = "UPDATE notas SET mensaje='$nota',usuario='$use',reg=NOW()";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function configticket(){
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function configticketupdate($titulo,$mensaje1,$mensaje2,$fuente,$tamano,$margsup){
        $strq = "UPDATE ticket SET titulo='$titulo',mensaje='$mensaje1',mensaje2='$mensaje2',fuente='$fuente',tamano='$tamano',margensup='$margsup'";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function marcashuevo_all(){
        $strq = "SELECT * FROM marca where marcaid<5";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
   

}