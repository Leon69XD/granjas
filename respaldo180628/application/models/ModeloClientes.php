<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class Modeloclientes extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function filas() {
        $strq = "SELECT COUNT(*) as total FROM clientes where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT * FROM clientes where activo=1 LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function clientesallsearch($usu){
        $strq = "SELECT * FROM clientes where activo=1 and Nom like '%".$usu."%' ORDER BY Nom ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    
    public function clientesinsert($nom,$correo,$calle,$nint,$next,$col,$loc,$muni,$cp,$esta,$pais,$contac,$correoc,$tel,$ext,$nexte,$des){
            $strq = "INSERT INTO clientes(Nom, Calle, noExterior, Colonia, Localidad, Municipio, Estado, Pais, CodigoPostal, Correo, noInterior, nombrec, correoc, telefonoc, extencionc, nextelc, descripcionc)   VALUES ('$nom','$calle','$next','$col','$loc','$muni','$esta','$pais','$cp','$correo','$nint','$contac','$correoc','$tel','$ext','$nexte','$des')";
            $this->db->query($strq);
            $id=$this->db->insert_id();
            return $id;
    }
    public function clientesupdate($id,$nom,$correo,$calle,$nint,$next,$col,$loc,$muni,$cp,$esta,$pais,$contac,$correoc,$tel,$ext,$nexte,$des){
            $strq = "UPDATE clientes SET Nom='$nom',Calle='$calle',noExterior='$nint',Colonia='$col',Localidad='$loc',Municipio='$muni',Estado='$esta',Pais='$pais',CodigoPostal='$cp',Correo='$correo',noInterior='$nint',nombrec='$contac',correoc='$correoc',telefonoc='$tel',extencionc='$ext',nextelc='$nexte',descripcionc='$des' WHERE ClientesId=$id";
            $this->db->query($strq);
    }
    function getcliente($id){
        $strq = "SELECT * FROM clientes where ClientesId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function deleteclientes($id){
        $strq = "UPDATE clientes SET activo=0 WHERE ClientesId=$id";
        $this->db->query($strq);
    }
}