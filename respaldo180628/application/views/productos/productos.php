<style type="text/css">
  .imgpro{
    width: 80px;
  }
  .imgpro:hover {
    transform: scale(2.6); 
    filter: drop-shadow(5px 9px 12px #444);
}
</style>
<div class="row">
                <div class="col-md-12">
                  <h2>Producto</h2>
                  <?php //echo $productosp;?>
                </div>
                
                <div class="col-md-12">
                  <div class="col-md-10"></div>
                  <div class="col-md-2">
                    <a href="<?php echo base_url(); ?>Productos/productosadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
                    <button class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow" onclick="productosall()"><i class="fa fa-print"></i></button>
                  </div>
                </div>
                
                
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de productos</h4>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-8">
                            
                          </div>
                          <div class="col-md-3">
                              <form role="search" class="navbar-form navbar-right mt-1">
                                <div class="position-relative has-icon-right">
                                  <input type="text" placeholder="Buscar" id="buscarpro" class="form-control round" oninput="buscarproducto()">
                                  <div class="form-control-position"><i class="ft-search"></i></div>
                                </div>
                              </form>
                          </div>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <!--------//////////////-------->
                                <table class="table table-striped" id="data-tables" style="width: 100%">
                                      <thead>
                                        <tr>
                                          <th></th>
                                          <th>Nombre</th>
                                          <th>Descripción</th>
                                          <th>Categoría</th>
                                          <th>Precio venta</th>
                                          <th>Existencia</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbodyresultadospro">

                                        <?php foreach ($productos->result() as $item){ ?>
                                         <tr id="trpro_<?php echo $item->productoid; ?>">
                                                  <td>
                                                    <?php 
                                                      if ($item->img=='') {
                                                        $img='public/img/ops.png';
                                                      }else{
                                                        $img=$item->img;
                                                      }
                                                    ?>
                                                    <img src="<?php echo base_url(); ?><?php echo $img; ?>" class="imgpro" >
                                                    </td>
                                                  <td><?php echo $item->nombre; ?></td>
                                                  <td><?php echo $item->descripcion; ?></td>
                                                  <td><?php echo $item->categoria; ?></td>
                                                  <td><?php echo $item->precioventa; ?></td>
                                                  <td><?php echo $item->stock; ?></td>
                                                  <td>
                                                      <div class="btn-group mr-1 mb-1">
                                                      <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                                                      <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                          <span class="sr-only">Toggle Dropdown</span>
                                                      </button>
                                                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                          <a class="dropdown-item" href="<?php echo base_url();?>Productos/productosadd?id=<?php echo $item->productoid; ?>">Editar</a>
                                                          <a class="dropdown-item" onclick="etiquetas(<?php echo $item->productoid; ?>,'<?php echo $item->codigo; ?>','<?php echo $item->nombre; ?>','<?php echo $item->categoria; ?>',<?php echo $item->precioventa; ?>);"href="#">Etiquetas</a>
                                                          <a class="dropdown-item" onclick="productodelete(<?php echo $item->productoid; ?>);"href="#">Eliminar</a>
                                                      </div>
                                                  </div>
                                                  </td>
                                                </tr>
                                        <?php } ?>
                                      </tbody>
                                    </table>
                                    <table class="table table-striped" id="data-tables2" style="display: none;width: 100%">
                                      <thead>
                                        <tr>
                                          <th></th>
                                          <th>Nombre</th>
                                          <th>Descripción</th>
                                          <th>Categoría</th>
                                          <th>Precio venta</th>
                                          <th>Existencia</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbodyresultadospro2">
                                    </tbody>
                                    </table>

                                    <div class="col-md-12">
                                      <div class="col-md-7">
                                        
                                      </div>
                                      <div class="col-md-5">
                                        <?php echo $this->pagination->create_links() ?>
                                      </div> 
                                    </div>
                        <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
<!------------------------------------------------>
<script type="text/javascript">
        $(document).ready(function() {
                $('#data-tabless').dataTable();
                $('#imprimiretiqueta').click(function(event) {
                  var idp=$('#idproetiqueta').val();
                  var nump=$('#numprint').val();
                  $('#iframeetiqueta').html('<iframe src="<?php echo base_url(); ?>Etiquetas?id='+idp+'&page='+nump+'&print=true"></iframe>');
                });
                $('#sieliminar').click(function(event) {
                  var idp =$('#hddIdpro').val();
                    $.ajax({
                        type:'POST',
                        url: '<?php echo base_url(); ?>Productos/deleteproductos',
                        data: {id:idp},
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr.error('Error!', 'No Se encuentra el archivo');
                            },
                            500: function(){
                                toastr.error('Error', '500');
                            }
                        },
                        success:function(data){
                            console.log(data);
                            //location.reload();
                            toastr.success('Hecho!', 'eliminado Correctamente');
                            var row = document.getElementById('trpro_'+idp);
                                        row.parentNode.removeChild(row);
                            
                        }
                    });
                });
        } );
        function productodelete(id){
          $('#hddIdpro').val(id);
          $('#eliminacion').modal();
          
      }
      function etiquetas(id,codigo,nombre,categoria,precio){
        $('#ecodigo').html(codigo);
        $('#eproducto').html(nombre);
        $('#ecategoria').html(categoria);
        $('#eprecio').html(precio);
        $('#idproetiqueta').val(id);

        $("#modaletiquetas").modal();
        $('#iframeetiqueta').html('<iframe src="<?php echo base_url(); ?>Etiquetas?id='+id+'&page=1"></iframe>'); 
      }
      function productosall(){
        $("#modalproductos").modal();
        $('#iframeproductos').html('<iframe src="<?php echo base_url(); ?>Visorpdf?filex=Productosall&iden=id&id=0" style="height: 500px "></iframe>'); 
      }
</script>
<style type="text/css">
  iframe{
        width: 100%;
        height: 300px;
        border: 0;
  }
</style>
<div class="modal fade text-left" id="modaletiquetas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Etiquetas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="col-md-6">
                <p>Codigo de producto: <b><span id="ecodigo"></span></b> </p>
                <p>Producto: <b><span id="eproducto"></span></b></p>
                <p>Categoria: <b><span id="ecategoria"></span></b></p>
                <p>Precio: <b>$ <span id="eprecio"></span></b></p>
                <input type="hidden" name="idproetiqueta" id="idproetiqueta" readonly>
                <div class="col-md-12">
                  <div class="form-group">
                    <div class=" col-md-6">
                        <input type="number" name="numprint" id="numprint" class="form-control" min="1" value="1">
                    </div>
                    <div class=" col-md-5">
                      <a href="#" class="btn btn-raised gradient-purple-bliss white" id="imprimiretiqueta">Imprimir</a>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-md-6" id="iframeetiqueta">
                
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="eliminacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmaci&oacute;n</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>Eliminar</b> el producto ?
                      <input type="hidden" id="hddIdpro">
            </div>
            <div class="modal-footer">
                <button type="button" id="sieliminar" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modalproductos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Productos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
              <div class="col-md-12" id="iframeproductos">
                
              </div>

           <!-- </div>-->
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>