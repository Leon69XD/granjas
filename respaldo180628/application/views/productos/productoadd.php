<?php 
if (isset($_GET['id'])) {
    $id=$_GET['id'];
    $personalv=$this->ModeloProductos->getproducto($id);
    foreach ($personalv->result() as $item){
      $label='Editar Producto';
      $id = $item->productoid;
      $codigo = $item->codigo;
      $productofiscal = $item->productofiscal;
      $nombre = $item->nombre;
      $descripcion = $item->descripcion;
      $categoria = $item->categoria;
      $stock = $item->stock;
      $preciocompra = $item->preciocompra;
      $porcentaje = $item->porcentaje;
      $precioventa = $item->precioventa;
      $mediomayoreo = $item->mediomayoreo;
      $canmediomayoreo = $item->canmediomayoreo;
      $mayoreo = $item->mayoreo;
      $canmayoreo = $item->canmayoreo;
      $img = '<img src="'.base_url().''.$item->img.'">' ;
  }
}else{
  $label='Nuevo producto';
  $id=0;
  $productoid='';
  $codigo='';
  $productofiscal='';
  $nombre='';
  $descripcion='';
  $categoria='';
  $stock='';
  $preciocompra='';
  $porcentaje=0;
  $precioventa='';
  $mediomayoreo='';
  $canmediomayoreo='';
  $mayoreo='';
  $canmayoreo='';
  $img='';     
} 
?>
<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
    margin-bottom: 5px;
  }
  .vd_green{
    color: #009688; 
  }
  input,select,textarea{
    margin-bottom: 15px;
  }
</style>
<input type="hidden" name="productoid" id="productoid" value="<?php echo $id;?>">
<div class="row">
                <div class="col-md-12">
                  <h2>Producto</h2>
                </div>
              </div>
              <!--Statistics cards Ends-->
              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><?php echo $label;?></h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block form-horizontal">
                                <!--------//////////////-------->
                                <div class="row">
                                  <form method="post"  role="form" id="formproductos">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Producto:</label>
                                          <div class=" col-md-4">
                                            <select class="form-control" id="pcategoria" name="pcategoria">
                                              <?php foreach ($categorias->result() as $item){ ?>      
                                                <option value="<?php echo $item->categoriaId; ?>" <?php if ($item->categoriaId==$categoria) { echo 'selected';} ?>    ><?php echo $item->categoria; ?></option>
                                              <?php } ?>
                                            </select>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Marca:</label>
                                          <div class=" col-md-4">
                                            <select class="form-control" id="pmarca" name="pmarca">
                                              <?php foreach ($marcaall->result() as $item){ ?>      
                                                <option value="<?php echo $item->marcaid; ?>"    ><?php echo $item->marca; ?></option>
                                              <?php } ?>
                                            </select>
                                          </div>
                                        </div>
                                    </div>  
                                    <div class="col-md-12"><br><br></div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Presentacion base:</label>
                                          <div class=" col-md-3">
                                            <select class="form-control" id="ppresentacion" name="ppresentacion">
                                              <option>CAJA</option>
                                              <option>1/2 CAJA</option>
                                              <option>BULTO</option>
                                              <option>1 LT CJA</option>
                                              <option>1/2 LT CJA</option>
                                              <option>CAJA de 5 LT</option>
                                              <option>4 PZA DE GALON</option>
                                              <option>CUBETA 20 LT</option>
                                              <option>CACHORRO</option>
                                              <option>ADULTO</option>
                                              <option>10 KG BULTO</option>
                                              <option>20 KG BULTO</option>
                                              <option>44 KG BULTO</option>
                                              <option>50 KG BULTO</option>
                                              <option>10 PIEZAS</option>
                                              <option>6 PIEZAS</option>
                                              <option>CAJA 6/3</option>
                                              <option>PZ 3 KG</option>
                                              <option>CJ 24/380</option>
                                              <option>CJ 40/200</option>
                                              <option>40/100</option>
                                              <option>48/200</option>
                                            </select>
                                          </div>
                                          <label class="col-md-1 control-label">UNIDAD</label>
                                          <div class=" col-md-2">
                                            <input type="number" name="" class="form-control">
                                          </div>
                                          <label class="col-md-1 control-label">Precio</label>
                                          <div class=" col-md-2">
                                            <input type="number" name="" class="form-control">
                                          </div>
                                          <div class=" col-md-1">
                                            <a href="#" class="btn btn-raised btn-primary" id="" ><i class="fa fa-plus"></i></a>
                                          </div>
                                        </div>
                                      
                                    </div>

















                                    <!--
                                      <div class="col-md-12">
                                          <div class="form-group">
                                            <label class="col-md-2 control-label">Codigo:</label>
                                            <div class=" col-md-4">
                                              <input type="text" class="form-control" name="pcodigo" id="pcodigo" value="<?php echo $codigo;?>" aria-describedby="button-addon2">
                                             
                                            </div>
                                            <div class="col-md-2">
                                              <a href="#" class="btn btn-raised btn-primary" id="generacodigo" >Generar</a>
                                            </div>
                                            <div class="col-md-3">
                                              <input type="checkbox" name="productofiscal" id="productofiscal" <?php  if($productofiscal==1){ echo 'checked';}?> >
                                              <label for="productofiscal">Producto fiscal</label>
                                              
                                            </div>

                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Nombre:</label>
                                          <div class=" col-md-10 ">
                                            <input type="text" class="form-control" id="productoname" name="productoname" value="<?php echo $nombre;?>">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Descripcion:</label>
                                          <div class=" col-md-10 ">
                                            <textarea class="form-control" id="pdescripcion" name="pdescripcion"><?php echo $descripcion;?></textarea>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Stock:</label>
                                          <div class=" col-md-10">
                                            <input type="number" class="form-control" id="pstock" name="pstock" value="<?php echo $stock;?>" placeholder="unidades">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    
                                    <div class="col-md-5">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; border: solid #a6a9ae 1px; border-radius: 12px;">
                                              <?php echo $img;?>
                                            </div>
                                            <div>
                                                <span class="btn btn-primary btn-file">
                                                    <span class="fileinput-new">Foto</span>
                                                    <span class="fileinput-exists">Cambiar</span>
                                                    <input type="file" name="imgpro" id="imgpro" data-allowed-file-extensions='["jpg", "png"]'>
                                                </span>
                                                <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                            </div>
                                        </div>
                                    </div>-
                                    <div class="col-md-12">
                                      <h3>Precios</h3>
                                      <hr />
                                      <div class="col-md-12">
                                          <div class="form-group">
                                            <label class="col-md-2 control-label">Precio Compra:</label>
                                            <div class=" col-md-3">
                                              <input type="number" name="preciocompra" id="preciocompra" class="form-control" value="<?php echo $preciocompra;?>" placeholder="">
                                            </div>
                                            <div class=" col-md-1" style="width: 47px;">
                                              <button class="btn btn-raised">%</button>
                                            </div>
                                            <div class=" col-md-2">
                                              <input type="number" class="form-control" name="porcentaje" id="porcentaje"  aria-describedby="button-addon2" onchange="calcular();" value="<?php echo $porcentaje;?>">
                                            </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="form-group">
                                            <label class="col-md-2 control-label">Precio venta:</label>
                                            <div class=" col-md-3">
                                              <input type="number" class="form-control" name="precioventa" id="precioventa" value="<?php echo $precioventa;?>" placeholder="">
                                            </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="form-group">
                                            <label class="col-md-2 control-label">Precio medio mayoreo:</label>
                                            <div class=" col-md-3">
                                              <input type="number" class="form-control" name="preciommayoreo" id="preciommayoreo" placeholder="" value="<?php echo $mediomayoreo;?>">
                                            </div>
                                            <div class=" col-md-2 ">
                                              
                                              <input type="number" class="form-control" name="cpmmayoreo" id="cpmmayoreo" value="<?php echo $canmediomayoreo;?>"  placeholder="a partir de # de unidades">
                                            </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="form-group">
                                            <label class="col-md-2 control-label">Precio mayoreo:</label>
                                            <div class=" col-md-3">
                                              <input type="number" class="form-control" name="preciomayoreo" id="preciomayoreo" value="<?php echo $mayoreo;?>" placeholder="">
                                            </div>
                                            <div class=" col-md-2">
                                              <input type="number" class="form-control" name="cpmayoreo" id="cpmayoreo" value="<?php echo $canmayoreo;?>" placeholder="a partir de # de unidades">
                                            </div>
                                          </div>
                                      </div>




                                    </div>-->
                                  </form>
                                  <div class="col-md-12">
                                    <button class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover"  type="submit"  id="savepr">Guardar</button>
                                  </div>






















                                  
                    
                                        
                                  
                                        
                                  
                                  
                                </div>
                                
                                
                                <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <script type="text/javascript">
                $(document).ready(function() {
                        $('#generacodigo').click(function(event) {
                          var d = new Date();
                          var dia=d.getDate();//1 31
                          var dias=d.getDay();//0 6
                          var mes = d.getMonth();//0 11
                          var yy = d.getFullYear();//9999
                          var hr = d.getHours();//0 24
                          var min = d.getMinutes();//0 59
                          var seg = d.getSeconds();//0 59
                          var yyy = 18;
                          var ram = Math.floor((Math.random() * 10) + 1);
                          var codigo=seg+''+min+''+hr+''+yyy+''+ram+''+mes+''+dia+''+dias;
                          //var condigo0=condigo.substr(0,13);
                          //console.log(codigo);
                          $('#pcodigo').val(codigo);
                        });
                });
              </script>