<div class="wrapper">
      <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper"><!--Registration Page Starts-->
            <section id="regestration">
                <div class="container">
                    <div class="row full-height-vh">
                        <div class="col-12 d-flex align-items-center justify-content-center">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row d-flex">
                                        <div class="col-12 col-sm-12 col-md-6" style="background: white">
                                            <div class="card-block">
                                                    <img style="margin-top: 16%" alt="Card image cap" src="<?php echo base_url(); ?>public/img/ops.png" width="280" height="180">
                                                <h2 class="card-title font-large-1 text-center white mt-3"></h2>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-6 d-flex align-items-center">
                                            <div class="card-block mx-auto">
                                                <br>
                                                <form class="form-horizontal" id="login-form" action="#" role="form">
                                                    <div class="form-group input-group mb-3">
                                                        <span class="input-group-addon">
                                                            <i class="icon-user"></i>
                                                        </span>
                                                        <input type="text" placeholder="Nombre de usuario" id="txtUsuario" name="txtUsuario" class="form-control required" required>
                                                    </div>
                                                    <div class="form-group input-group mb-3">
                                                        <span class="input-group-addon">
                                                            <i class="ft-lock"></i>
                                                        </span>
                                                        <input type="password" placeholder="Contrase&ntilde;a" id="txtPass" name="txtPass" class="form-control required" required>
                                                    </div>
                                                    <div class=" text-center">
                                                      <button type="submit" class="btn btn-raised gradient-bloody-mary white shadow-big-navbar" id="login-submit">Iniciar Sesión</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<!--Registration Page Ends-->
          </div>
        </div>
      </div>
    </div>