<div class="row">
    <div class="col-md-12">
      <h2>Ventas </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Ventas de mostrador</h4>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row match-height panelventa1">
                  <?php foreach ($categoriasll->result() as $item){ 
                               if($item->img==''){
                                            $imgcat='public/img/ops.png';
                                          }else{
                                            $imgcat='public/img/categoria/'.$item->img;
                    
                                          }

                              ?>
                        <div class="col-lg-3 col-md-3 col-sm-12 mb-2" onclick="producto(<?php echo $item->categoriaId; ?>)">
                            <div class="card card-inverse bg-primary text-center" style="height: 300.906px;">
                                <div class="card-body">
                                    <div class="card-img overlap">
                                        <img src="<?php echo base_url(); ?><?php echo $imgcat; ?>" alt="element 06" width="225" class="mb-1">
                                    </div>
                                    <div class="card-block">
                                        <h4 class="card-title"><?php echo $item->categoria; ?></h4>
                                        <!--<p class="card-text">Donut toffee candy brownie soufflé macaroon.</p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                  <?php } ?>
                </div>
                <div class="row panelventa2" style="display: none;">
                    <?php foreach ($marcasll->result() as $item){ 
                               if($item->imgm==''){
                                            $imgmar='public/img/ops.png';
                                          }else{
                                            $imgmar='public/img/marcas/'.$item->imgm;
                                          }

                              ?>
                        <div class="col-lg-3 col-md-3 col-sm-12 mb-2" onclick="marcasselec(<?php echo $item->marcaid; ?>)">
                            <div class="card card-inverse bg-primary text-center" style="height: 300.906px;">
                                <div class="card-body">
                                    <div class="card-img overlap">
                                        <img src="<?php echo base_url(); ?><?php echo $imgmar; ?>" alt="element 06" width="225" class="mb-1">
                                    </div>
                                    <div class="card-block">
                                        <h4 class="card-title"><?php echo $item->marca; ?></h4>
                                        <!--<p class="card-text">Donut toffee candy brownie soufflé macaroon.</p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                  <?php } ?>
                </div>
                <div class="row panelventa3" style="display: none;">
                    
                        <div class="col-lg-3 col-md-3 col-sm-12 mb-2" onclick="marcasselec()">
                            <div class="card card-inverse bg-primary text-center" style="height: 300.906px;">
                                <div class="card-body">
                                    
                                    <div class="card-block">
                                        <h4 class="card-title">Cajas</h4>
                                        <!--<p class="card-text">Donut toffee candy brownie soufflé macaroon.</p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                 
                </div>
                <div class="row panelventa4" style="display: none;">
                  <div class="col-md-12">
                            <div class="col-md-1 selectimg">
                              <img src="<?php echo base_url(); ?>public/img/categoriat/180531-113616catHuevo.jpg" width="100px">
                            </div>
                            <div class="col-md-1">
                              <input type="hidden" name="seembarquedId" id="seembarquedId" class="form-control" readonly>
                              <input type="hidden" name="seproductoId" id="seproductoId" class="form-control" readonly>
                              <input type="text" name="senomproducto" id="senomproducto" value="huevo" readonly style="width: 132px;border: 0px;">
                            </div>
                            <div class="col-md-4">
                              <input type="hidden" name="secategoriaId" id="secategoriaId" readonly >
                              <input type="text" name="secategoria" id="secategoria" value="calvario" readonly style="width: 104px;border: 0px;">
                              <input type="tel" name="secosto" id="secosto" value="100" readonly style="width: 82px;">
                              <input type="text" name="seclave" id="seclave" readonly style="width: 150px;border: 0px;">

                            </div>
                            
                        </div>
                          <div class="bloque4 col-md-6">
                            <div class="row">
                              <div class="controls col-md-12">
                                <div class="input-group">
                                  <input type="text" class="form-control" placeholder="cantidad" id="cantidadcajas" name="cantidadcajas" aria-describedby="button-addon2">
                                  <div class="input-group-append">
                                    <span class="input-group-btn" id="button-addon2">
                                      <button class="btn btn-raised btn-primary" type="button" onclick="pulsadaenter()">Agregar</button>
                                    </span>
                                  </div>
                                </div>
                                
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group ">
                                      <input type="checkbox" id="ticketventa" class="switchery" data-size="lg" data-on-text="SI" data-off-text="NO" checked/>
                                      <label for="ticketventa" class="font-medium-2 text-bold-600 ml-1">Ticket venta</label>
                                      
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group ">
                                      <input type="checkbox" id="ticketrecoleccion" class="switchery" data-size="lg" data-on-text="SI" data-off-text="NO" checked/>
                                      <label for="ticketrecoleccion" class="font-medium-2 text-bold-600 ml-1">Ticket Recolección</label>
                                      
                                  </div>
                              </div>
                            </div>

                            
                            
                            <div class="row">
                              <div class="col-md-6">
                                <div class="col-md-12 btn btn-raised gradient-crystal-clear white" id="realizarventa">
                                  <i class="fa fa-shopping-cart fa-2x"></i> Registrar venta
                                </div>
                              </div>
                              <div class="col-md-1"></div>
                              <div class="col-md-5">
                                <div class="col-md-12 btn btn-raised gradient-ibiza-sunset white" data-toggle="modal" data-target="#modalconfirm">
                                  <i class="fa fa-ban"></i> Cancelar
                                </div>
                              </div>
                            </div>
                            
                          </div>
                          <div class="bloque4 col-md-6">
                              <div class="row">
                                <table class="table table-striped table-hover no-head-border" id="tableproductos">
                                  <thead class="vd_bg-grey vd_white">
                                    <tr>
                                    <td>Producto</td>
                                    <td>Categoria</td>
                                    <td>Clave</td>
                                    <td>Precio</td>
                                    <td>Cantidad</td>
                                    <td>Total</td>
                                    </tr>
                                  </thead>
                                  <tbody id="addproductos">
                                  
                                  </tbody>
                                </table>
                              </div>
                              <div class="row" style="text-align: center;">
                                <a class="btn btn-raised gradient-purple-bliss white masproductos"><i class="fa fa-plus fa-fw "></i></a>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="col-md-offset-6">
                                    <p>cuenta</p>
                                    <h2 id="totalpro">$ 0.00</h2>
                                    <input type="hidden" id="totalprod" value="0">
                                  </div>
                                </div>
                              </div>
                          </div>
                </div>
                <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>






<!----------------------------------------------------------------------------------------=====-->
<style type="text/css">
  .bloques{
    cursor: pointer; 
    width: 23%; 
    padding-left: 1%; 
    padding-right: 1%;
    float: left;
    margin: 1%;
  }
  .bloquecalcu{
    width: 30%;
    margin: 1%;
    padding: 10px;
    float: left;
    text-align: center;
    font-size: 20px;
    color: white;
    cursor: pointer;
  }
  .logobanana{
    filter: drop-shadow(5px 9px 12px #444);
  }
  .productos{
    max-width: 361px
    width:80%;
  }
  .addproductoss{
    background: transparent;
    border: 0px !important;
  }
  .bloquecalcu:active{
    background-color: #c55d0d !important;
  }
  .ticketventa{
    width: 100%;
    border: 0px;
    height: 500px;
  }
  .tablenumber{
    background: white;
  }
  .numbertouch{
    font-size: 29px;
    padding-left: 35px;
    padding-right: 35px;
  }
  .numbertouch{
    font-size: 25px;
    padding-left: 35px;
    padding-right: 35px;
  }

</style>
<input type="hidden" name="usuarioid" id="usuarioid" value="0">


<div class="modal fade" id="modalconfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header vd_bg-grey vd_white">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="myModalLabel">Confirmar</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <h2>¿Desea confirmar la cancelacion?</h2>          
          </div>
        </div>
      </div>
      <div class="modal-footer background-login">
        <button type="button" class="btn vd_btn vd_bg-red" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn vd_btn vd_bg-green" data-dismiss="modal" onclick="location.reload()">Confirmar</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<div class="modal fade" id="modalimpresion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg"> 
    <div class="modal-content">
      <div class="modal-header vd_bg-grey vd_white">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h4 class="modal-title" id="myModalLabel">Ticket</h4>
      </div>
      <div class="modal-body" style="padding-top: 0px;padding-bottom: 0px;">
        <div class="row"id="iframeticket">
          
        </div>
      </div>
      <div class="modal-footer background-login">
        <button type="button" class="btn vd_btn vd_bg-green" data-dismiss="modal" onclick="location.reload();">Cerrar</button>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/switchery.min.css">
<script src="<?php echo base_url(); ?>app-assets/vendors/js/switchery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>app-assets/js/switch.min.js" type="text/javascript"></script>