var base_url=$('#base_url_g').val();
var table;
$(document).ready(function () {
        table = $('#tableproductosa').DataTable();
        loadtable();
        $('#pcategoria').select2({
            placeholder: {
                id: 0,
                text: 'Seleccione un producto'
            },
            width: '100%',
            allowClear: true
        });
        //$("#pcategoria").val(0).change();
        $('#pmarca').select2({
            placeholder: {
                id: 0,
                text: 'Seleccione una marca'
            },
            width: '100%',
            allowClear: true
        });
        //$("#pmarca").val(0).change();
        $('#ppresentacion').select2({
            width: '100%',
            minimumInputLength: 3,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una presentación base',
            ajax: {
                url: $('#base_url').val()+'/searchpres',
                dataType: "json",
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data){
                    var itemspre = [];
                    data.forEach(function(element) {
                        itemspre.push({
                            id: element.presentacionId,
                            text: element.presentacion,
                            unidad: element.unidad
                        });
                    });
                    return {
                        results: itemspre
                    };          
                },  
            }
        });

        $('#savepr').click(function(event) {
            $.ajax({
                    type:'POST',
                    url: 'productoadd',
                    data: {
                        id: $('#productoid').val(),
                        prod: $('#pcategoria option:selected').val(),
                        marca: $('#pmarca option:selected').val(),
                        compra: $('#pcompra').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){toastr.error('Error!', 'No Se encuentra el archivo');},
                        500: function(data){toastr.error('Error', data);}
                    },
                    success:function(data){
                        var idpro=data;
                        
                        var DATA  = [];
                        var TABLA   = $("#productosv tbody > tr");
                            TABLA.each(function(){         
                                item = {};
                                item ["idpro"] = idpro;
                                item ["ppresentacionid"]   = $(this).find("input[id*='ppresentacionid']").val();
                                item ["presentacion"]   = $(this).find("select[id*='ppresentacion']").val();
                                item ["cantidad"]  = $(this).find("input[id*='precantidad']").val();
                                item ["precio"]  = $(this).find("input[id*='preprecio']").val();
                                item ["apartirmm"]  = $(this).find("input[id*='apartirmm']").val();
                                item ["prepreciomm"]  = $(this).find("input[id*='prepreciomm']").val();
                                item ["apartirm"]  = $(this).find("input[id*='apartirm0']").val();
                                item ["prepreciom"]  = $(this).find("input[id*='prepreciom0']").val();

                                item ["cantidad2"]  = $(this).find("input[id*='precantidad2']").val();
                                item ["precio2"]  = $(this).find("input[id*='preprecio2']").val();
                                item ["apartirmm2"]  = $(this).find("input[id*='apartirmm2']").val();
                                item ["prepreciomm2"]  = $(this).find("input[id*='prepreciomm2']").val();
                                item ["apartirm2"]  = $(this).find("input[id*='apartirm2']").val();
                                item ["prepreciom2"]  = $(this).find("input[id*='prepreciom2']").val();

                                item ["cantidad3"]  = $(this).find("input[id*='precantidad3']").val();
                                item ["precio3"]  = $(this).find("input[id*='preprecio3']").val();
                                item ["apartirmm3"]  = $(this).find("input[id*='apartirmm3']").val();
                                item ["prepreciomm3"]  = $(this).find("input[id*='prepreciomm3']").val();
                                item ["apartirm3"]  = $(this).find("input[id*='apartirm3']").val();
                                item ["prepreciom3"]  = $(this).find("input[id*='prepreciom3']").val();

                                item ["tipo"]  = $(this).find("input[id*='tipo']").val();
                                DATA.push(item);
                            });
                            INFO  = new FormData();
                            aInfo   = JSON.stringify(DATA);
                            INFO.append('data', aInfo);
                            $.ajax({
                                data: INFO,
                                type: 'POST',
                                url : 'productoaddetalle',
                                processData: false, 
                                contentType: false,
                                async: false,
                                statusCode:{
                                    404: function(data){
                                        toastr.error('Error!', 'No Se encuentra el archivo');
                                    },
                                    500: function(data){
                                        toastr.error('Error', data);
                                    }
                                },
                                success: function(data){
                                    console.log(data);
                                    setInterval(function(){ 
                                        location.href='../Productos';
                                    }, 3000);
                                }
                            });
                            
                            toastr.success( 'Registro guardado','Hecho!');
                            
                    }
                });
        });
        $('#add_traspaso').click(function(event) {
            $.ajax({
                    type:'POST',
                    url: $('#base_url').val()+'traspasos',
                    data: {
                        id: $('#protraspaso').val(),
                        origen: $('#bodegatraspaso option:selected').val(),
                        destino: $('#bodegatraspaso2 option:selected').val(),
                        cant:$('#cantidadtraspaso').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){toastr.error('Error!', 'No Se encuentra el archivo');},
                        500: function(data){toastr.error('Error', data);}
                    },
                    success:function(data){
                        loadtable();
                        console.log(data);
                        toastr.success( 'Traspaso realizado','Hecho!');
                    }
                });
        });
        var rowproductos=1;
        $('#addsubproductos').click(function(event) {
                var addpro='<tr class="classproducto_'+rowproductos+'">';
                    addpro+='<td>';
                      addpro+='<select class="form-control precentacionrow_'+rowproductos+'" id="ppresentacion" name="ppresentacion"></select> <input type="hidden" id="ppresentacionid" value="0">';
                    addpro+='</td>';
                    addpro+='<td>';
                        addpro+='<input type="number" name="" id="precantidad1" class="form-control precantidad_1_'+rowproductos+'" readonly>';
                        addpro+='<input type="number" name="" id="precantidad2" class="form-control precantidad_2_'+rowproductos+'" readonly>';
                        addpro+='<input type="number" name="" id="precantidad3" class="form-control precantidad_3_'+rowproductos+'" readonly>';
                    addpro+='</td>';
                    addpro+='<td>';
                        addpro+='<input type="number" name="" id="preprecio" class="form-control preprecio_1_'+rowproductos+'">';
                        addpro+='<input type="number" name="" id="preprecio2" class="form-control preprecio_2_'+rowproductos+'">';
                        addpro+='<input type="number" name="" id="preprecio3" class="form-control preprecio_3_'+rowproductos+'">';
                    addpro+='</td>';
                    addpro+='<td>';
                        addpro+='<input type="number" name="" id="apartirmm" class="form-control apartirmm_1_'+rowproductos+'">';
                        addpro+='<input type="number" name="" id="apartirmm2" class="form-control apartirmm_2_'+rowproductos+'">';
                        addpro+='<input type="number" name="" id="apartirmm3" class="form-control apartirmm_3_'+rowproductos+'">';
                    addpro+='</td>';
                    addpro+='<td>';
                        addpro+='<input type="number" name="" id="prepreciomm" class="form-control prepreciomm_1_'+rowproductos+'">';
                        addpro+='<input type="number" name="" id="prepreciomm2" class="form-control prepreciomm_2_'+rowproductos+'">';
                        addpro+='<input type="number" name="" id="prepreciomm3" class="form-control prepreciomm_3_'+rowproductos+'">';
                    addpro+='</td>';
                    addpro+='<td>';
                        addpro+='<input type="number" name="" id="apartirm0" class="form-control apartirm_1_'+rowproductos+'">';
                        addpro+='<input type="number" name="" id="apartirm2" class="form-control apartirm_2_'+rowproductos+'">';
                        addpro+='<input type="number" name="" id="apartirm3" class="form-control apartirm_3_'+rowproductos+'">';
                    addpro+='</td>';
                    addpro+='<td>';
                        addpro+='<input type="number" name="" id="prepreciom0" class="form-control prepreciom_1_'+rowproductos+'">';
                        addpro+='<input type="number" name="" id="prepreciom2" class="form-control prepreciom_2_'+rowproductos+'">';
                        addpro+='<input type="number" name="" id="prepreciom3" class="form-control prepreciom_3_'+rowproductos+'">';
                        addpro+='<input type="hidden" id="tipo" value="0">';



                    addpro+='</td>';
                    addpro+='<td>';
                      addpro+='<a href="#" class="btn btn-raised btn-danger" onclick="removerow('+rowproductos+')"  ><i class="fa fa-minus"></i></a>';
                    addpro+='</td>';
                  addpro+='</tr>';
                $('#divaddproductos').append(addpro);
                cargardatospresentacion(rowproductos);
                rowproductos++;
        });
        $('#add_merma').click(function(event) {
            $.ajax({
                    type:'POST',
                    url: $('#base_url').val()+'mermaadd',
                    data: {
                        id: $('#promermerma').val(),
                        marca: $('#marcamerma option:selected').val(), //tipo merma
                        press: $('#presmerma option:selected').val(), //presentacion merma
                        cant: $('#cantidadmerma').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){toastr.error('Error!', 'No Se encuentra el archivo');},
                        500: function(data){toastr.error('Error', data);}
                    },
                    success:function(data){
                        console.log(data);
                        toastr.success( 'Registro guardado','Hecho!');   
                        setTimeout(function(){ 
                            //location.reload(); 
                        }, 2000); 
                    }
                });
        });
});
function calcular(){
	var costo = $('#preciocompra').val();
    var porcentaje = $('#porcentaje').val();
    var porcentaje2 = porcentaje/100;
    var costo2 = costo*porcentaje2;
    var cantitotal = parseFloat(costo)+parseFloat(costo2);
    $('#precioventa').val(cantitotal);
    $('#preciommayoreo').val(cantitotal);
    $('#cpmmayoreo').val('1');
    $('#preciomayoreo').val(cantitotal);
    $('#cpmayoreo').val('1');
}
function buscarproducto(){
    var search=$('#buscarpro').val();
    if (search.length>2) {
        $.ajax({
            type:'POST',
            url: $('#base_url').val()+'buscarpro',
            data: {
                buscar: $('#buscarpro').val()
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#tbodyresultadospro2').html(data);
            }
        });
        $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
    }else{
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
    }
}
function cargardatospresentacion(id){
    $('.precentacionrow_'+id).select2({
            width: '100%',
            minimumInputLength: 3,
            minimumResultsForSearch: 10,
            placeholder: 'Buscar una presentación',
            ajax: {
                url: 'searchpres',
                dataType: "json",
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }
                    return query;
                },
                processResults: function(data){
                    var itemspre = [];
                    data.forEach(function(element) {
                        itemspre.push({
                            id: element.presentacionId,
                            text: element.presentacion,
                            unidad:element.unidad
                        });
                    });
                    return {
                        results: itemspre
                    };          
                },  
            }
        }).on('select2:select', function (e) {
        var data = e.params.data;
        console.log(data);
        //addproducto();
        var cantidad = $('.precantidad_1_0').val();
        var unidad=data.unidad;
        var total=parseFloat(cantidad)/parseFloat(unidad);
        $('.precantidad_1_'+id).val(total.toFixed(0));

        var cantidad = $('.precantidad_2_0').val();
        var unidad=data.unidad;
        var total=parseFloat(cantidad)/parseFloat(unidad);
        $('.precantidad_2_'+id).val(total.toFixed(0));

        var cantidad = $('.precantidad_3_0').val();
        var unidad=data.unidad;
        var total=parseFloat(cantidad)/parseFloat(unidad);
        $('.precantidad_3_'+id).val(total.toFixed(0));
    });;
}
function removerow(id){
    $('.classproducto_'+id).remove();
}

function calcularn(){
    var cantidadb=$('.precantidad_1_0').val();
    var TABLA   = $("#productosv tbody > tr");
        TABLA.each(function(){  
            var unidad=$(this).find("input[id*='unidad']").val()==undefined?1:$(this).find("input[id*='unidad']").val();
            var stockt=parseFloat(cantidadb)/parseFloat(unidad);
            $(this).find("input[id*='precantidad1']").val(stockt);
        });
}
function calcularn2(){
    var cantidadb=$('.precantidad_2_0').val();
    var TABLA   = $("#productosv tbody > tr");
        TABLA.each(function(){  
            var unidad=$(this).find("input[id*='unidad']").val()==undefined?1:$(this).find("input[id*='unidad']").val();
            var stockt=parseFloat(cantidadb)/parseFloat(unidad);
            $(this).find("input[id*='precantidad2']").val(stockt);
        });
}
function calcularn3(){
    var cantidadb=$('.precantidad_3_0').val();
    var TABLA   = $("#productosv tbody > tr");
        TABLA.each(function(){  
            var unidad=$(this).find("input[id*='unidad']").val()==undefined?1:$(this).find("input[id*='unidad']").val();
            var stockt=parseFloat(cantidadb)/parseFloat(unidad);
            $(this).find("input[id*='precantidad3']").val(stockt);
        });
}

function loadtable(){
    table.destroy();
    table = $('#tableproductosa').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Productos/getlistproductos",
            type: "post",
            "data": {
                'ro':'0'
            },

        },
        "columns": [
            {"data": "productoaddId"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.img==''){
                        var img='public/img/ops.png';
                    }else{
                        var img='public/img/categoriat/'+row.img;
                    }

                        html ='<img src="'+base_url+''+img+'" class="imgpro" >';
                    return html;
                }
            },
            {"data": "categoria"},
            {"data": "marca"},
            {"data": "stok"},
            {"data": "stok2"},
            {"data": "stok3"},
            {"data": "productoaddId",
                render:function(data,type,row){
                    var html='';
                        html+='<div class="btn-group mr-1 mb-1">\
                                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                      <span class="sr-only">Toggle Dropdown</span>\
                                  </button>\
                                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                      <a class="dropdown-item" href="'+base_url+'Productos/productosadd?id='+row.productoaddId+'">Editar</a>'
                                      if (row.categoriaId==1) {
                                        html+='<a class="dropdown-item" onclick="mermas(<?php echo $item->productoaddId; ?>);"href="#">Mermas</a>';
                                      }
                                      html+='<a class="dropdown-item" onclick="traspaso('+row.productoaddId+','+row.stok+','+row.stok2+','+row.stok3+');"href="#">Traspaso</a>\
                                      <a class="dropdown-item" onclick="productodelete('+row.productoaddId+');"href="#">Eliminar</a>\
                                  </div>\
                              </div>';
                    return html;
                }
            },
            
            
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10,25, 50, 100], [10,25, 50,100]]
        
    });
}